# F5_AWAF_GitOPs

```mermaid
graph LR
  A[Git App Repo] -->B(AS3 Payload) --> C[BIG-IP]
  D[WAF Repo] -->|AS3 Pull| B
  C -->|WAF Updates| D
```


## AWAF webhook

```
 "webhooks": [
            {
              "body": "payload={\"channel\": \"#slackChannel\", \"text\": \"Thwarting the Rebel scum! An apply has been made:\n on Device: [{{device.hostname}}] Policy: [{{policy.name}}]\"}",
              "contentType": "application/x-www-form-urlencoded",
              "name": "chat_hook",
              "triggerEvent": "apply-policy",
              "url": "https://hooks.slack.com/services/<token from slack>"
            },
            {
              "body": "payload={\"channel\": \"#slackChannel\", \"text\": \"Malicious Request:\n on Device: [{{device.hostname}}] Policy: [{{policy.name}}], Client IP: [{{request.clientIp}}], Method: [{{request.method}}], Viol Rating [{{request.rating}}], Event ID [{{request.id]]}\"}",
              "contentType": "application/x-www-form-urlencoded",
              "name": "mal_chat",
              "triggerEvent": "http-request-likely-malicious",
              "url": "https://hooks.slack.com/services/<token from slack>"
            },
            {
              "body": "{\"token\": \"<insert token>\",\"ref\": \"<git branch>\",\"variables\": {\"POLICY_NAME\": \"{{policy.name}}\", \"HOST_NAME\": \"{{device.hostname}}\", \"F5_IP\": \"<BIG-IP IP Address>\"}}",
              "contentType": "application/json",
              "name": "waf_hook",
              "triggerEvent": "apply-policy",
              "url": "http://gitlab.fqdn/api/v4/projects/1/trigger/pipeline"
            }
          ],

```
This is an exert of the declarative WAF policy deployed to the BIG-IP. In the body section of the 3rd webhook you can insert
- pipeline trigger token (provided by pipeline trigger)
- reference branch (provided by pipeline trigger)
- variables 


## .gitlab-ci

In these steps the gitlab-runner is using ssh-keyscan to get the ssh fingerprint of the Gitlab host and adding it to it's known_hosts file. It is also 
importing the CICD Variable created **SSH_PUSH_KEY** and assigning it to the file *id_rsa*
```
- ssh-keyscan ${CI_SERVER_HOST} > ~/.ssh/known_hosts
- echo "${SSH_PUSH_KEY}" > ~/.ssh/id_rsa
- chmod 400 ~/.ssh/id_rsa
```
Setting up some basic Git configuratin items. Insert your email and Gitlab username as this is used in the commit process. The *git remote add* command is creating a remote (your Gitlab hosted repository) and giving it the alias name of **ssh_origin**. The Gitlab predefined variables can be found [here](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html). In this use case *CI_SERVER_HOST:CI_PROJECT_PATH* would be equal to *gitlab.com:cwise/f5_awaf_gitops.git*. Also, setting the timezone to my local timezone as this will be used in our commit message.
```
- git config user.email "<your email>"
- git config user.name "<gitlab username>"
- git remote add ssh_origin "git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git"
- export TZ=America/Los_Angeles
```
Using git and the aliased remote (*ssh_origin*), the runner will now pull the current project to local, then execut the **sync** bash scripts to get the modified WAF policy. For the *git diff-index* I won't dive to deep on this but I would suggest looking into the term [Porcelian vs Plumbing](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain). This concept behind *git diff-index* is to check "if" there are changes to the repository (did the policy get updated). If there was no update git diff-index outpus a *0* and quietly exits.   
IF there is a change, *git diff-index* ouputs a *1* and the OR `||` action get implemented. The files are then staged with commit message (current date time). 
The commit is then pushed up to the remote.  The *retry* detemines how many times the Gitlab job will run if it fails.

```
  - git pull ssh_origin main
  - source ./sync
  - git diff-index --quiet HEAD || git commit -am "sync waf_pol `date`"
  - git push ssh_origin HEAD:main
retry: 2
```

Here *rules* are defined telling the CI process when to run. In this scenario the CI job executes only when a *trigger* event is sent and there is a job timeout of 10 minutes.
```
rules:
  - if: $CI_PIPELINE_SOURCE == "trigger"
    when: always
timeout: 10m
```

## sync

This is a fairly simple bash script. Once the pipeline is triggered the script will login to the F5 and get the policy reference ID based on the policy name. Next is to extract a mininmal version of the updated policy in *json* format. This conversion can take a little time and the *while* is there to wait for a **COMPLETED** return from the F5 and download the newly updated policy file.
